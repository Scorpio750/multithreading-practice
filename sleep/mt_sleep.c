#include "mt_sleep.h"


void *foo(void *arg) {
    char *str_input = (char*)arg;

    printf("You passed in %s\n", str_input);
    return NULL;
}

void *bar(void *arg) {
    const int SLEEP_TIME = (int)arg;
    int rand_sleep_time;

    for (;;) {
        rand_sleep_time = rand() % ((SLEEP_TIME / 10) + 1 - 0) + 0;
        printf("Sleeping for %d\n", rand_sleep_time);
        sleep(rand_sleep_time);
        printf("bar\n");
    }
    return NULL;
}

int main(int argc, char **argv) {
    const int SLEEP_TIME = 30;

    // allocates thread_1 space on the C stack
    // C stack is user-scoped memory
    // we then zero out all that memory
    pthread_t thread_1 = {0};
    pthread_create(&thread_1, NULL, bar, (void *)(size_t)SLEEP_TIME);

    pthread_t thread_2 = {0};
    pthread_create(&thread_2, NULL, foo, "foo");
    sleep(SLEEP_TIME);
}
